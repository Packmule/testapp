﻿using System;
using System.Collections.Generic;

namespace TestApp.Domain
{
    public class GreetingService
    {
        private static readonly Random randomizer = new Random();

        private readonly List<string> greetings = new List<string>() 
        {
            "Hello",
            "Hi",
            "Howzit",
            "Aloha",
            "Good day",
            "Bonjour",
            "Sup",
            "Howdy"
        };

        private readonly List<string> usedGreetings = new List<string>();

        public string GetRandomGreeting()
        {
            if (greetings.Count == 0)
            {
                greetings.AddRange(usedGreetings);
                usedGreetings.RemoveAll(s => usedGreetings.Contains(s));
            }

            var greetingIndex = GetRandomNumber(0, greetings.Count);
            var selectedGreeting = greetings[greetingIndex];

            usedGreetings.Add(selectedGreeting);
            greetings.RemoveAt(greetingIndex);

            return selectedGreeting;
        }

        private int GetRandomNumber(double minimum, double maximum)
        {
            return (int)(randomizer.NextDouble() * (maximum - minimum) + minimum);
        }
    }
}
