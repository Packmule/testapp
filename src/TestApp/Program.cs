﻿using System;
using TestApp.Domain;

namespace TestApp
{
    class Program
    {
        private static readonly GreetingService greetingService = new GreetingService();

        static void Main(string[] args)
        {
            bool greetAgain;
            do
            {
                Greet();
                greetAgain = Console.ReadLine().Length > 0;
            }
            while (greetAgain);
        }


        private static void Greet()
        {
            Console.WriteLine($"{greetingService.GetRandomGreeting()} World!");
        }
    }
}
